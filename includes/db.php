<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 22/02/2018
 * Time: 10:32 PM
 */

namespace stelab\includes\db;

class db
{

    protected $host = 'localhost';
    protected $db = 'stelab';
    protected $username = 'root';
    protected $password = '';
    protected $Connection = '';

    function __construct(){
        $Connection = new \PDO("mysql:dbname={$this->db};host={$this->host}", $this->username, $this->password);
    }

    public function TableInfo($TableName = "", $FieldID = "", $FieldValue = "", $ExtraQuery = "", $QVal = array(), $Select = "")
    {
        $Connection = new \PDO("mysql:dbname={$this->db};host={$this->host}", $this->username, $this->password);


        $Select = $Select ?: "*";

        if (is_array($Select)) {
            $Select = implode(", ", $Select);
        }

        $FieldID = preg_replace("/[^a-zA-Z0-9_()]/", "", $FieldID);
        $FieldID2 = preg_replace("/[^a-zA-Z0-9_]/", "", $FieldID);
        $TableName = preg_replace("/[^a-zA-Z0-9_]/", "", $TableName);
//        $TableName = self::getTable($TableName);

        if ($TableName && ($FieldID || ($ExtraQuery))) {

            $ExtraQuery .= " LIMIT 1";

            if ($FieldID) {
                $ExtraValues[$FieldID2] = $FieldValue;

                $sth = $Connection->prepare("SELECT {$Select} FROM {$TableName} WHERE {$FieldID} = :{$FieldID2} {$ExtraQuery}");

            } else {
                $sth = $Connection->prepare("SELECT {$Select} FROM {$TableName} WHERE 1 {$ExtraQuery}");
            }

            $sth->execute($ExtraValues);


            return $data = $sth->fetch(\PDO::FETCH_ASSOC);
//            print_r( $data = $sth->fetch(\PDO::FETCH_ASSOC));
        }

        return false;


    }

    public function GroupInfo($TableName = "", $FieldID = "", $FieldValue = "", $ExtraQuery = "", $QVal = array(), $Select = "")
    {
        $Connection = new \PDO("mysql:dbname={$this->db};host={$this->host}", $this->username, $this->password);


        $Select = $Select ?: "*";

        if (is_array($Select)) {
            $Select = implode(", ", $Select);
        }

        $FieldID = preg_replace("/[^a-zA-Z0-9_()]/", "", $FieldID);
        $FieldID2 = preg_replace("/[^a-zA-Z0-9_]/", "", $FieldID);
        $TableName = preg_replace("/[^a-zA-Z0-9_]/", "", $TableName);
//        $TableName = self::getTable($TableName);

        if ($TableName && ($FieldID || ($ExtraQuery))) {


            if ($FieldID) {
                $ExtraValues[$FieldID2] = $FieldValue;

                $sth = $Connection->prepare("SELECT {$Select} FROM {$TableName} WHERE {$FieldID} = :{$FieldID2} {$ExtraQuery}");

            } else {
                $sth = $Connection->prepare("SELECT {$Select} FROM {$TableName} WHERE 1 {$ExtraQuery}");
            }

            $sth->execute($ExtraValues);

            return $data = $sth->fetchAll(\PDO::FETCH_ASSOC);
        }

        return false;


    }


}