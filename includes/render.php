<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 22/02/2018
 * Time: 10:01 AM
 */

/**
 * Class render
 */
class render
{
    static function input($data = false)
    {
        if ($data && !is_array($data)) {
            $data = false;
        }

        if (!$data && count($_POST) >= 1) {
            $data = $_POST;
        } else if (!$data) {
            $data = $_GET;
        }

        return $data;
    }

    static function tplrender($document, $data)
    {

        $loader = new Twig_Loader_Filesystem('_views/');
        $twig = new Twig_Environment($loader,[]);

        return $twig->render($document,$data);

    }

    static function get_ip()
    {
        //Just get the headers if we can or else use the SERVER global
        if (function_exists('apache_request_headers')) {
            $headers = apache_request_headers();
        } else {
            $headers = $_SERVER;
        }
        //Get the forwarded IP if it exists
        if (array_key_exists('X-Forwarded-For', $headers) && filter_var($headers['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            $the_ip = $headers['X-Forwarded-For'];
        } elseif (array_key_exists('HTTP_X_FORWARDED_FOR', $headers) && filter_var($headers['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)
        ) {
            $the_ip = $headers['HTTP_X_FORWARDED_FOR'];
        } else {

            $the_ip = filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
        }
        return $the_ip;
    }

}