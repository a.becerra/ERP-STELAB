<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!---->

<nav class="navbar navbar-toggleable-md navbar-light bg-faded  navbar navbar-inverse bg-primary">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">STELAB</a>
    <div class="collapse navbar-collapse " id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="../myaccount/dashboard.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../myaccount/notes.php">Notas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Pricing</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Administraci&oacute;n
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="javascript:void(0);" onclick="location.href = '../myaccount/users.php/?cmd=users'">Usuarios</a>
                    <a class="dropdown-item" href="javascript:void(0);" onclick="location.href = '../myaccount/users.php/?cmd=group'">Grupos</a>
                </div>
            </li>
        </ul>

    </div>
    <span class="navbar-text pull-right" style="margin-right: 20px;">

        <?php

        if(!isset($_SESSION))

        {
            session_start();
        }

        echo $_SESSION['UserFirstName'];

        ?>

    </span>
    <i class="fa fa-user"></i>

</nav>

<br>

<script>

    $("#LogOut").on("click",function () {
        /*function getLoginValidate(UserID, UserPassword) {

            var parametros = {
                "UserID"        : UserID,
                "UserPassword"    : UserPassword
            };


            $.ajax({
                data:  parametros,
                url:   'validate.php',
                type:  'post',
                beforeSend: function () {
                    $("#Alert").removeClass("hidden");
                    $("#Alert").html("Procesando, espere por favor...");
                },
                success:  function (response) {

                    if (response == 1){
                        window.location.href = "../myaccount/dashboard.php";
                    }
                    else {
                        $("#Alert").html(response);
                    }
                }
            });

        }
        location.reload();*/
    });

</script>
