<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 21/02/2018
 * Time: 10:05 PM
 */

$UsersObj = new users();
$UsersObj->init();


class users{

    protected $db;
    protected $tpldata;

    function init(){

        require_once("../../vendor/autoload.php");
        include ('../../includes/render.php');
        include ('../../includes/db.php');

        $this->db = new stelab\includes\db\db();


        session_start();

        if (!$_SESSION['UserID']){
//            include("_views/dashboard.html");
            header("Location: ../login/login.php");

        }

        $this->index();

    }

    function index(){


        $UserInfo = $this->db->TableInfo("st_users", "UserID", $_SESSION['UserID']);

        $this->tpldata['name'] = $UserInfo['UserFirstName'];


//        $loader = new Twig_Loader_Filesystem('_views/');
//        $twig = new Twig_Environment($loader,[]);


        echo render::tplrender("dashboard.html",$this->tpldata);

//        echo $twig->render('dashboard.html',$this->tpldata);

    }



}

//    echo $_SESSION['UserID'];

?>