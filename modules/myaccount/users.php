<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 21/02/2018
 * Time: 10:05 PM
 */

$UserObj = new users();
$UserObj->init();


class users{

    protected $db;
    protected $tpldata;

    function init(){

        require_once("../../vendor/autoload.php");
        include ('../../includes/render.php');
        include ('../../includes/db.php');
        include('../../templates/nav.php');

        $this->db = new stelab\includes\db\db();

        if (!$_SESSION['UserID']){
            header("Location: ../login/login.php");
        }

        if ($_GET['cmd'] == "users"){
            $this->cmd_users();
        }
        elseif ($_GET['cmd'] == "groups"){
            echo "groups";
        }
        else{
            echo "error";
        }

//        $this->index();

    }

    function cmd_users(){


        $UserInfo = $this->db->TableInfo("st_users", "UserID", $_SESSION['UserID']);

        $UsersGroup = $this->db->GroupInfo("st_users","Cancelled","0");

        $this->tpldata['name'] = $UserInfo['UserFirstName'];
        $this->tpldata['UserGroup'] = $UsersGroup;

        echo render::tplrender("users/users.php",$this->tpldata);

    }

}

?>