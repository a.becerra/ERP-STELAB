


<div class="container" style="margin-top: 20px;">


    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Nombre</th>
            <th scope="col">Correo</th>
            <th scope="col">Creaci&oacute;n</th>
        </tr>
        </thead>

        {% for UserInfo in UserGroup %}

            <tbody>
                <tr>
                    <th scope="row">{{ UserInfo.UserID }} </th>
                    <td>{{ UserInfo.UserFullName }} </td>
                    <td>{{ UserInfo.UserEmail }}</td>
                    <td>{{ UserInfo.DocDate|date("m/d/Y") }}</td>
                    <td>

                        <div class="dropdown ">
                            <a class="btn btn-secondary " style="border: none" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                <i class="fa fa-bars"></i>

                            </a>

                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="#"><i class="fa fa-edit">&nbsp;Editar</i></a>
                                <a class="dropdown-item" href="#"><i class="fa fa-trash"></i>&nbsp;Eliminar</a>
                            </div>
                        </div>

                    </td>
                </tr>


            </tbody>

        {% endfor %}


    </table>


</div>
