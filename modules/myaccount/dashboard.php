<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 21/02/2018
 * Time: 10:05 PM
 */

$DashboardObj = new dashboard();
$DashboardObj->init();


class dashboard{

    protected $db;
    protected $tpldata;

    function init(){

        require_once("../../vendor/autoload.php");
        include ('../../includes/render.php');
        include ('../../includes/db.php');
        include('../../templates/nav.php');

        $this->db = new stelab\includes\db\db();

//        session_start();

        if (!$_SESSION['UserID']){
            header("Location: ../login/login.php");
        }

        $this->index();

    }

    function index(){

        $UserInfo = $this->db->TableInfo("st_users", "UserID", $_SESSION['UserID']);

        $this->tpldata['name'] = $UserInfo['UserFirstName'];

        echo render::tplrender("dashboard.php",$this->tpldata);

    }

}

?>